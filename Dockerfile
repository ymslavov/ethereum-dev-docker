#
# Ubuntu Dockerfile
#
# https://github.com/dockerfile/ubuntu
#

# Pull base image.
FROM ubuntu:16.04

# Install.
RUN \
  apt-get autoremove && apt-get autoclean && \
  apt-get update && \
  apt-get -y upgrade && \
  apt-get install -y build-essential && \
  apt-get install -y software-properties-common && \
  apt-get install -y byobu curl git htop man unzip vim wget sudo && \
  add-apt-repository -y ppa:ethereum/ethereum && \
  apt-get update && \
  apt-get install -y ethereum && \
  rm -rf /var/lib/apt/lists/*

# Create non-root user
RUN groupadd -g 1000 yasen && \
    useradd -u 1000 -g yasen -m yasen --shell /bin/bash && \
    adduser yasen sudo && \
    echo "yasen ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers && \
    passwd -dl yasen && \
    apt-get update -yqq

# Install npm
RUN \
    curl -sL https://deb.nodesource.com/setup_8.x | bash - && \
    apt-get install -y nodejs && \
    npm i ethereumjs-testrpc web3 solc && \
    npm i -g truffle webpack

EXPOSE 8545 8080

# Define default command.
CMD ["bash"]